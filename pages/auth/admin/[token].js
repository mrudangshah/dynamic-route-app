import React, { useEffect } from 'react';
import styles from '../../../styles/Home.module.css'
import { useRouter } from 'next/router'
import axios from "axios";

export default function AdminToken({data}) {
  
  const router = data;
  const _token = router.token;
  const redirectRouter = useRouter()

  useEffect(() => {
    if(localStorage.getItem("userName")) {
      localStorage.removeItem('userName');
      axios.post(process.env.API_ENDPOINT+`/admin-login/redirect`, {
        token: _token
      })
      .then((response) => {
        localStorage.setItem("token", response.data.data.token);
        localStorage.setItem("userName", "John Doe");
      }).then(() => {
        redirectRouter.push("/")
      })
    } else {
      axios.post(process.env.API_ENDPOINT+`/admin-login/redirect`, {
        token: _token
      })
      .then((response) => {
        localStorage.setItem("token", response.data.data.token);
        localStorage.setItem("userName", "John Doe");
      }).then(() => {
        redirectRouter.push("/")
      })
    }
  })

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h3 className={styles.description}>
          Loading...!!!
        </h3>
      </main>
    </div>
  );
}

export async function getServerSideProps(context) {
	const data = context.query
  console.log(data)
	if (!data) {
    return {
      notFound: true,
    }
  }
	return { props: { data } }
}